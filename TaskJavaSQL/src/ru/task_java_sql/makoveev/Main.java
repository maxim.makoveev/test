package ru.task_java_sql.makoveev;

import java.sql.*;

public class Main {

    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "qwerty007";
    public static Connection conn = null;
    public static Statement stmt = null;
    //language=SQL
    private static final String SQL_DEPT_SALARY_QUERY = "select department, sum(salary) as total_salary from employees group by department";

    public static void main(String[] args) {
        //Open a connection
        System.out.println("Connecting to database");
        try {
            conn = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
            System.out.println("Connected successfully");
            checkForExistingValues();
            if (stmt != null)
                conn.close();
            if (conn != null)
                conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            throw new IllegalArgumentException();
        }
        System.out.println("----------");
        System.out.println("Finished");
    }

    private static void checkForExistingValues() throws SQLException {
        //Check if "employee" table is there
        DatabaseMetaData dbm = conn.getMetaData();
        ResultSet tables = dbm.getTables(null, null, "employees", null);
        if (tables.next()) {
            System.out.println("Table 'employees' already exists");
        } else {
            System.out.println("Creating table 'employees' in given database");
            createTable();
        }
        //Execute a query
        makeQuery();
    }

    private static void createTable() {
        try {
            stmt = conn.createStatement();
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }

        //language=PostgreSQL
        String tableEmployees = "CREATE TABLE EMPLOYEES " +
                "(EMPLOYEE_ID BIGSERIAL not NULL, " +
                " FIRST_NAME VARCHAR(30) not NULL, " +
                " LAST_NAME VARCHAR(30) not NULL, " +
                " DEPARTMENT VARCHAR(20) not NULL, " +
                " SALARY INTEGER not NULL, " +
                " PRIMARY KEY (EMPLOYEE_ID))";

        try {
            stmt.executeUpdate(tableEmployees);
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
        System.out.println("Table created");
        System.out.println("----------");
        System.out.println("Populating table with values");

        //language=SQL
        String insertEmployee = " insert into employees(first_name, last_name, department, salary) " +
                "values ('John', 'Smith', 'Development', 5000), ('Nick', 'Johnson', 'Development', 6000)," +
                " ('Mary', 'Johnson', 'Sales', 4000), ('Cristopher', 'Robin', 'Sales', 4000)," +
                " ('Harry', 'Gates', 'Management', 8000)";
        try {
            stmt.executeUpdate(insertEmployee);
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
        System.out.println("Done");
    }

    private static void makeQuery() {
        System.out.println("----------");
        System.out.println("Executing query:");
        try {
            stmt = conn.createStatement();
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
        try (ResultSet rows = stmt.executeQuery(SQL_DEPT_SALARY_QUERY)) {
            while (rows.next()) {
                System.out.println(rows.getString("department") + ": "
                        + rows.getString("total_salary"));
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }
}
