# TASK JAVA SQL

Java программа, которая выполняет запрос SQL из таблицы базы данных и выводит результат в консоль IDE.
Для создания и чтения данных использована технология JDBC.

## Задача:

вывести на экран (консоль) список отделов с суммарной зарплатой сотрудников отдела.

## Результат:
файл .java с исходным кодом программы. 

## Тестовые данные
* База данных: PostgreSQL 13.2, localhost
* IDE: IntelliJ IDEA Ultimate 2021.1 
* SDK: Amazon Corretto 16
* ### Таблица EMPLOYEES

EMPLOYEE_ID | FIRST_NAME | LAST_NAME    | DEPARTMENT   | SALARY
----------- | -----------|--------------| -------------|----------
1           | John       | Smith        | Development  |	5000
2           | Nick       | Johnson      | Development  |	6000
3           | Mary       | Johnson      | Sales        |	4000
4           | Cristopher | Robin        | Sales        |	4000
5           | Harry       | Gates       | Management   |	8000


## Лицензия
Программа распространяется свободно под лицензией [MIT](https://choosealicense.com/licenses/mit/)